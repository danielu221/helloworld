package org.apache.maven;


import static junit.framework.Assert.assertEquals;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class HelloWorldTest
{
	
	public void testMessage()
	{
		assertEquals("Hello World!"  , HelloWorld.message());
	}
	
}

